<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePmTables extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create( 'venues', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->string('name');
            $table->string( 'slug' )->unique();
            $table->text('address');

            $table->double('latitude', 10, 6)->nullable();
            $table->double('longitude', 10, 6)->nullable();

            // Multi-tenancy
            $table->integer( 'conference_id' )->unsigned()->nullable();
            $table->foreign( 'conference_id' )->references( 'id' )->on( 'conferences' )->onDelete( 'cascade' );

            // All the other data
            $table->longText( 'data' );

            $table->timestamps();
        });

        Schema::create( 'rooms', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->string( 'name' );
            $table->integer( 'capacity' )->unsigned();

            $table->string( 'slug' )->unique();

            // Multi-tenancy
            $table->integer( 'conference_id' )->unsigned()->nullable();
            $table->foreign( 'conference_id' )->references( 'id' )->on( 'conferences' )->onDelete( 'cascade' );

            // All the other data
            $table->longText( 'data' );

            // Venue id if venues table exist
            $table->integer( 'venue_id' )->unsigned()->nullable();
            $table->foreign( 'venue_id' )->references( 'id' )->on( 'venues' )->onDelete( 'set null' );

            $table->timestamps();
        } );

        Schema::create( 'sessions', function ( Blueprint $table ) {
            $table->increments( 'id' );

            // Session type
            $table->integer( 'session_type_id' )->unsigned()->nullable();
            $table->foreign( 'session_type_id' )->references( 'id' )->on( 'session_types' )->onDelete( 'set null' );

            // Topic
            $table->integer( 'topic_id' )->unsigned()->nullable();
            $table->foreign( 'topic_id' )->references( 'id' )->on( 'topics' );

            // Multi-tenancy
            $table->integer( 'conference_id' )->unsigned()->nullable();
            $table->foreign( 'conference_id' )->references( 'id' )->on( 'conferences' )->onDelete( 'cascade' );

            // All the other data
            $table->longText( 'data' );

            $table->timestamps();
        } );

        Schema::create( 'events', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->string( 'name' );

            $table->string( 'slug' )->unique();

            $table->dateTime( 'start_time' );
            $table->dateTime( 'end_time' );

            $table->boolean('secondary')->default(false);

            $table->integer( 'room_id' )->unsigned()->nullable();
            $table->foreign( 'room_id' )->references( 'id' )->on( 'rooms' )->onDelete( 'cascade' );

            $table->integer( 'session_id' )->unsigned()->nullable();
            $table->foreign( 'session_id' )->references( 'id' )->on( 'sessions' )->onDelete( 'cascade' );

            // Multi-tenancy
            $table->integer( 'conference_id' )->unsigned()->nullable();
            $table->foreign( 'conference_id' )->references( 'id' )->on( 'conferences' )->onDelete( 'cascade' );

            // All the other data
            $table->longText( 'data' );

            $table->timestamps();
        } );

        // Sessions submissions table only if table exists
        if ( Schema::hasTable( 'submissions' ) ) {
            Schema::create( 'sessions_submissions', function ( Blueprint $table ) {
                $table->increments( 'id' );

                $table->integer( 'session_id' )->unsigned();
                $table->foreign( 'session_id' )->references( 'id' )->on( 'sessions' )->onDelete( 'cascade' );

                $table->integer( 'submission_id' )->unsigned();
                $table->foreign( 'submission_id' )->references( 'id' )->on( 'submissions' )->onDelete( 'cascade' );

                $table->integer('position');

                $table->timestamps();
            } );
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'sessions_submissions' );
        Schema::drop( 'events' );
        Schema::drop( 'sessions' );
        Schema::drop( 'rooms' );
        Schema::drop( 'venues' );
    }
}
