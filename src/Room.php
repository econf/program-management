<?php

namespace EConf\ProgramManagement;

use App\Traits\HasDataFieldTrait;
use App\Traits\TenantableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Room extends Model {

    use TenantableTrait;
    use HasDataFieldTrait;

    use Sluggable;
    use SluggableScopeHelpers;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'capacity',
        'data',
        'venue_id'
    ];

    protected $casts = [
        'data' => 'array',
    ];

    public function venue() {
        return $this->belongsTo( Venue::class );
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName() {
        return 'slug';
    }
}
