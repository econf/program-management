<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 30/05/16
 * Time: 18:56
 */

namespace EConf\ProgramManagement\Assignment;


use App\SessionType;
use App\Topic;
use DB;
use EConf\ProgramManagement\Session;
use EConf\Reviews\Bid;
use EConf\Reviews\Http\Controllers\ProgramCommitteeController;
use EConf\Submissions\Submission;
use EConf\Submissions\SubmissionHelpers;
use Setting;

class Chronos extends Algorithm {

    protected $name = "Chronos";
    protected $description = "This algorithm distributes the highly scored submissions by topic by the sessions, gropuing submission with the same topics.";

    /**
     * @inheritdoc
     */
    public function assign() {

        $topics = Topic::all();

        $accepted_submissions = Submission::select( DB::raw( 'submissions.*, avg(reviews.score) as score' ) )->leftJoin( 'reviews', 'submissions.id', '=', 'reviews.submission_id' )->groupBy( 'submissions.id' )->orderBy( 'score', 'desc' )->where( 'submissions.accepted', true )->get();

//        $accepted_submissions = Submission::whereAccepted( true )->get();
        $submissions = [ ];

        $sessions_submissions = [ ];

        foreach ( $topics as $topic ) {
            $topic_submissions = $topic->belongsToMany( Submission::class )->get();
            $submissions[$topic->id] = $accepted_submissions->whereIn( 'id', $topic_submissions->pluck( 'id' )->all() )->keyBy( 'id' );
        }

        $submissions = collect( $submissions );
        $sessions = Session::all()->keyBy( 'id' );

        while ( !$submissions->flatten( 1 )->isEmpty() && !$sessions->isEmpty() ) {

            $submissions = $submissions->sortByDesc( function ( $value, $key ) {
                return $value->count();
            } );

            $sessions = $sessions->sortByDesc( function ( Session $session, $key ) use ( $sessions_submissions ) {
                return $session->nSubmissionsInSession() - ( array_key_exists( $session->id, $sessions_submissions ) ? count( $sessions_submissions[$session->id] ) : 0 );
            } )->keyBy( 'id');

            $session = $sessions->first();

            $chosen_submissions = $submissions->first()->slice( 0, $session->nSubmissionsInSession() - ( array_key_exists( $session->id, $sessions_submissions ) ? count( $sessions_submissions[$session->id] ) : 0 ) );

            $sessions_submissions[$session->id] = array_merge( (isset($sessions_submissions[$session->id])?$sessions_submissions[$session->id]:[]), $chosen_submissions->pluck( 'id' )->all() );

            foreach ($chosen_submissions as $submission){
                foreach ($submissions as $topic_subs){
                    $topic_subs->forget($submission->id);
                }
            }

            if($session->nSubmissionsInSession() <= ( array_key_exists( $session->id, $sessions_submissions ) ? count( $sessions_submissions[$session->id] ) : 0 )){
                $sessions->forget($session->id);
            }

        }

        foreach ($sessions_submissions as $id => $session){
            shuffle($session);
            $sessions_submissions[$id] = $session;
        }

        return $sessions_submissions;

    }

}
