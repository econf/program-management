<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 30/05/16
 * Time: 18:56
 */

namespace EConf\ProgramManagement\Assignment;


use DB;
use EConf\ProgramManagement\Session;
use EConf\Reviews\Bid;
use EConf\Reviews\Http\Controllers\ProgramCommitteeController;
use EConf\Submissions\Submission;
use EConf\Submissions\SubmissionHelpers;
use Setting;

class Dummy extends Algorithm {

    protected $name = "Dummy Assignment";
    protected $description = "This algorithm just assigns the submissions in order, ignoring basically everything.";

    /**
     * @inheritdoc
     */
    public function assign() {

        $sessions = Session::select( DB::raw( 'sessions.*' ) )->join( 'events', 'sessions.id', '=', 'events.session_id' )->orderBy( 'events.start_time' )->get();

        $submissions = Submission::whereAccepted( true )->get();

        $assign = [ ];

        $subm_i = 0;

        foreach ( $sessions as $session ) {
            $num = $session->nSubmissionsInSession();
            $assign[$session->id] = $submissions->slice( $subm_i, $num )->pluck('id')->all();

            $subm_i += $num;

            if($submissions->count() < $subm_i){
                break;
            }
        }

        return $assign;

    }
}
