<?php

namespace EConf\ProgramManagement;

use App\Traits\HasDataFieldTrait;
use App\Traits\TenantableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

use Geocoder;

class Venue extends Model {

    use TenantableTrait;
    use HasDataFieldTrait;

    use Sluggable;
    use SluggableScopeHelpers;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'data'
    ];

    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @inheritDoc
     */
    protected static function boot() {
        parent::boot();

        self::saving( function ( Venue $venue ) {

            /**
             * @var $geocoder Geocoder\Geocoder
             */
            $geocoder = \App::make('geocoder');

            $coords = [null, null];

            try {
                $geocode = $geocoder->geocode( implode( ', ', explode( PHP_EOL, $venue->address ) ) );
                $coords = $geocode->getCoordinates();

            } catch ( \Exception $e ) {

                try {
                    $geocode = $geocoder->geocode( $venue->name );
                    $coords = $geocode->getCoordinates();
                } catch ( \Exception $e ) {
                    // If coordinates can not be found, ignore.
                }

            }

            $venue->latitude = $coords[0];
            $venue->longitude = $coords[1];

        } );
    }

    public function rooms(){
        return $this->hasMany(Room::class)->orderBy('name');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName() {
        return 'slug';
    }
}
