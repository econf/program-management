<?php

function pm_routes() {
    Route::group( [ 'namespace' => 'EConf\ProgramManagement\Http\Controllers', 'middleware' => ['web', 'locale'] ], function () {

        // Public routes
        Route::get('program', 'PublicController@program');
        Route::get('program/{slug}', 'PublicController@event');

        Route::group(['middleware' => 'auth'], function(){
            // Auth routes
        });

        Route::group(['prefix' => 'admin/program', 'middleware' => 'committee:organizing'], function(){
            Route::resource('rooms', 'Admin\RoomsController');
            Route::resource('venues', 'Admin\VenuesController');
            Route::resource('sessions', 'Admin\SessionsController');
            if(\EConf\ProgramManagement\ProgramHelpers::hasSubmissions()) {
                Route::put( 'sessions/{id}/submission', 'Admin\SessionsController@addSubmission' );
                Route::delete( 'sessions/{id}/submission/{subm_id}', 'Admin\SessionsController@removeSubmission' );
                Route::post( 'sessions/{id}/submission/{subm_id}', 'Admin\SessionsController@moveSubmission' );
                Route::get( 'assignment', 'Admin\AssignmentController@auto_show' );
                Route::get( 'assignment/{algo}', 'Admin\AssignmentController@auto_algo' );
                Route::post( 'assignment', 'Admin\AssignmentController@auto_store' );
            }
            Route::resource('events', 'Admin\EventsController');
        });
    } );
}

Route::group( [ 'namespace' => 'EConf\ProgramManagement\Http\Controllers', 'middleware' => [ 'web', 'locale' ] ], function () {

// Assets
    Route::get( 'assets/program.css', 'AssetsController@css' );
    //Route::get( 'assets/program.js', 'AssetsController@js' );

} );

if ( Config::get( 'econf.multi' ) ) {
    // Multi
    Route::group( [ 'prefix' => '{conf_slug}', 'middleware' => 'multi' ], function () {
        pm_routes();
    } );
} else {
    // Single
    pm_routes();
}

