<?php

// Program management
Breadcrumbs::register('program', function($breadcrumbs)
{
    $breadcrumbs->push(trans('program::program.label'));
});

// Program > Rooms
Breadcrumbs::register('program.rooms', function($breadcrumbs)
{
    $breadcrumbs->parent('program');
    $breadcrumbs->push(trans('program::program.rooms.label'), m_action('\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@index'));
});

// Program > Rooms > Add Room
Breadcrumbs::register('program.rooms.create', function($breadcrumbs)
{
    $breadcrumbs->parent('program.rooms');
    $breadcrumbs->push(trans('program::program.rooms.add_room'));
});

// Program > Rooms > Edit Room
Breadcrumbs::register('program.rooms.edit', function($breadcrumbs)
{
    $breadcrumbs->parent('program.rooms');
    $breadcrumbs->push(trans('program::program.rooms.edit_room'));
});

// Program > Rooms > Room
Breadcrumbs::register('program.rooms.show', function($breadcrumbs, $room)
{
    $breadcrumbs->parent('program.rooms');
    $breadcrumbs->push($room->name);
});

// Program > Venues
Breadcrumbs::register('program.venues', function($breadcrumbs)
{
    $breadcrumbs->parent('program');
    $breadcrumbs->push(trans('program::program.venues.label'), m_action('\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@index'));
});

// Program > Venues > Add Venue
Breadcrumbs::register('program.venues.create', function($breadcrumbs)
{
    $breadcrumbs->parent('program.venues');
    $breadcrumbs->push(trans('program::program.venues.add_venue'));
});

// Program > Venue > Edit Venue
Breadcrumbs::register('program.venues.edit', function($breadcrumbs)
{
    $breadcrumbs->parent('program.venues');
    $breadcrumbs->push(trans('program::program.venues.edit_venue'));
});

// Program > Venues > Venue
Breadcrumbs::register('program.venues.show', function($breadcrumbs, $venue)
{
    $breadcrumbs->parent('program.venues');
    $breadcrumbs->push($venue->name);
});

// Program > Sessions
Breadcrumbs::register('program.sessions', function($breadcrumbs)
{
    $breadcrumbs->parent('program');
    $breadcrumbs->push(trans('program::program.sessions.label'), m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@index'));
});

// Program > Sessions > Add Session
Breadcrumbs::register('program.sessions.create', function($breadcrumbs)
{
    $breadcrumbs->parent('program.sessions');
    $breadcrumbs->push(trans('program::program.sessions.add_session'));
});

// Program > Sessions > Edit Session
Breadcrumbs::register('program.sessions.edit', function($breadcrumbs)
{
    $breadcrumbs->parent('program.sessions');
    $breadcrumbs->push(trans('program::program.sessions.edit_session'));
});

// Program > Sessions > Session
Breadcrumbs::register('program.sessions.show', function($breadcrumbs)
{
    $breadcrumbs->parent('program.sessions');
    $breadcrumbs->push(trans('program::program.sessions.singular_label'));
});

// Program > Events
Breadcrumbs::register('program.events', function($breadcrumbs)
{
    $breadcrumbs->parent('program');
    $breadcrumbs->push(trans('program::program.events.label'), m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@index'));
});

// Program > Events > Add Event
Breadcrumbs::register('program.events.create', function($breadcrumbs)
{
    $breadcrumbs->parent('program.events');
    $breadcrumbs->push(trans('program::program.events.add_event'));
});

// Program > Events > Edit Event
Breadcrumbs::register('program.events.edit', function($breadcrumbs)
{
    $breadcrumbs->parent('program.events');
    $breadcrumbs->push(trans('program::program.events.edit_event'));
});

// Program > Events > Event
Breadcrumbs::register('program.events.show', function($breadcrumbs, $event)
{
    $breadcrumbs->parent('program.events');
    $breadcrumbs->push($event->name);
});

// Program > Assignment
Breadcrumbs::register('program.assignment', function($breadcrumbs)
{
    $breadcrumbs->parent('program');
    $breadcrumbs->push(trans('program::program.assignment.label'));
});
