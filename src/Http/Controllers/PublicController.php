<?php

namespace EConf\ProgramManagement\Http\Controllers;

use App\Http\Controllers\Controller;
use App\SessionType;
use App\Topic;
use Carbon\Carbon;
use EConf\ProgramManagement\Event;
use EConf\ProgramManagement\Room;
use EConf\ProgramManagement\Session;
use Eventy;
use Flash;
use DB;

use App\Http\Requests;

class PublicController extends Controller {

    public function program() {

        // Add assets
        Eventy::addFilter( 'public.assets', function ( $val ) {
            $val[] = action( '\EConf\ProgramManagement\Http\Controllers\AssetsController@css' );
            //$val[] = action( '\EConf\ProgramManagement\Http\Controllers\AssetsController@js' );
            return $val;
        }, 40, 1 );

        $program = [];

        $events = Event::select(DB::raw('*, DATE(start_time) as date'))->orderBy('start_time')->get()->groupBy('date');

        foreach ($events as $date => $day_evts){
            $day = [
                'date' => new Carbon($date),
                'events' => []
            ];

            $prev = null;
            $row = [];
            while(!$day_evts->isEmpty()){
                $ev = $day_evts->shift();
                if(!is_null($prev)){
                    if(!$prev->overlaps($ev)){
                        $day['events'][] = $row;
                        $row = [];
                    }
                }
                $prev = $ev;
                $row[] = $ev;
            }
            $day['events'][] = $row;

            $program[] = $day;
        }

        return view('program::public.program', compact('program'));
    }

    public function event($slug){

        $event = Event::findBySlugOrFail($slug);

        // Add assets
        Eventy::addFilter( 'public.assets', function ( $val ) {
            $val[] = 'bower_components/leaflet/dist/leaflet.css';
            $val[] = 'bower_components/leaflet/dist/leaflet.js';
            return $val;
        }, 40, 1 );

        return view('program::public.event', compact('event'));

    }

}
