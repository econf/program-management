<?php

namespace EConf\ProgramManagement\Http\Controllers;

use App\Committee;
use App\Http\Controllers\Controller;
use App\SessionType;
use Auth;
use Config;
use Doctrine\Common\CommonException;
use EConf\Reviews\Bid;
use EConf\Reviews\Conflict;
use EConf\Reviews\ConflictException;
use EConf\Reviews\Review;
use EConf\Reviews\ReviewHelpers;
use EConf\Submissions\Submission;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Setting;
use Flash;
use stdClass;

class AssetsController extends Controller {

    public function css() {

        $content = $this->dumpAssetsToString('css');

        $response = new Response(
            $content, 200, array(
                'Content-Type' => 'text/css',
            )
        );

        return $this->cacheResponse( $response );
    }

    public function js() {

        $content = $this->dumpAssetsToString('js');

        $response = new Response(
            $content, 200, array(
                'Content-Type' => 'text/javascript',
            )
        );

        return $this->cacheResponse( $response );
    }

    /**
     * Cache the response 1 year (31536000 sec)
     */
    protected function cacheResponse( Response $response ) {
        /*if ( !Config::get( 'app.debug', false ) ) {
            $response->setSharedMaxAge( 31536000 );
            $response->setMaxAge( 31536000 );
            $response->setExpires( new \DateTime( '+1 year' ) );
        }*/
        return $response;
    }

    private function dumpAssetsToString( $type ) {
        $files = $this->getAssets($type);
        $content = '';
        foreach ($files as $file) {
            $content .= file_get_contents($file) . "\n";
        }
        return $content;
    }

    private function getAssets( $type ) {
        $files = glob(__DIR__.'/../../resources/assets/'.$type.'/*.'.$type, GLOB_BRACE);
        return $files;
    }

}
