<?php

namespace EConf\ProgramManagement\Http\Controllers\Admin;

use App\Committee;
use App\Http\Controllers\Controller;
use App\SessionType;
use Asset;
use Auth;
use DB;
use Doctrine\Common\CommonException;
use EConf\ProgramManagement\Session;
use EConf\Reviews\Assignment\Algorithm;
use EConf\Reviews\Bid;
use EConf\Reviews\Conflict;
use EConf\Reviews\ConflictException;
use EConf\Reviews\Review;
use EConf\Reviews\ReviewHelpers;
use EConf\Submissions\Submission;
use Eventy;
use Illuminate\Http\Request;

use Setting;
use Flash;
use stdClass;

class AssignmentController extends Controller {

    public function auto_show() {

        $this->confirmAccess();

        $algo_array = Eventy::filter('program.assignment.algorithms');

        $algorithms = [];

        foreach ($algo_array as $algo){
            $algorithms[] = new $algo();
        }

        return view('program::admin.assignment.auto', compact( 'algorithms' ));
    }

    public function auto_algo($slug){

        $this->confirmAccess();

        $algos = Eventy::filter('program.assignment.algorithms', []);

        if(!array_key_exists( $slug, $algos )){
            Flash::error( 'Non-existant algorithm (translate this)' );
            return redirect(m_action( '\EConf\ProgramManagement\Http\Controllers\AssignmentController@auto_show' ));
        }

        $alg_class = $algos[$slug];

        /**
         * @var $algorithm Algorithm The assignment algorithm
         */
        $algorithm = new $alg_class();

        $assigns = $algorithm->assign();

        $sessions = Session::select( DB::raw( 'sessions.*' ) )->join( 'events', 'sessions.id', '=', 'events.session_id' )->orderBy( 'events.start_time' )->get()->keyBy( 'id');

        $submissions = Submission::whereAccepted( true )->get()->keyBy( 'id');

        $existing_assignments = false;

        return view('program::admin.assignment.algorithm', compact( 'algorithm', 'submissions', 'sessions', 'assigns', 'existing_assignments' ));

    }

    public function auto_store( Request $request ) {
        $this->storeAssignments( $request->asgn );

        Flash::success( trans( 'reviews::reviews.assignment.save_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\AssignmentController@auto_show' ) );
    }

    /**
     * @param $asgn
     * @throws \Exception
     */
    private function storeAssignments( $asgn ) {

        if(empty($asgn)){
            $asgn = [];
        }


        $sessions = Session::all();
        foreach ($sessions as $s){
            $s->submissions()->detach();
            if(array_key_exists( $s->id, $asgn)){
                $s->submissions()->attach(array_keys($asgn[$s->id]));
            }
        }


    }

    private function confirmAccess() {
        return true;
    }

}
