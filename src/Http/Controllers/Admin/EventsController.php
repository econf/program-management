<?php

namespace EConf\ProgramManagement\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Topic;
use Carbon\Carbon;
use EConf\ProgramManagement\Event;
use EConf\ProgramManagement\Room;
use Flash;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

class EventsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $events = Event::orderBy( 'events.start_time' )->get();
        return view( 'program::admin.events.index', compact( 'events' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $rooms = Room::orderBy( 'name' )->get();
        return view( 'program::admin.events.create', compact( 'rooms' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        $rooms = Room::select('id')->get()->pluck('id')->all();
        $rooms[] = "null";

        $this->validate( $request, [
            'name' => 'required',
            'start_time' => 'required|date',
            'end_time' => 'required|date',
            'room_id' => 'in:'.implode(",",$rooms),
            'data.description' => 'required_without:secondary',
        ] );

        $input = $request->all();
        $input['start_time'] = new Carbon($input['start_time']);
        $input['end_time'] = new Carbon($input['end_time']);
        if($input['room_id'] == 'null'){
            $input['room_id'] = null;
        }

        $event = Event::create( $input );

        Flash::success( trans( 'program::program.events.add_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@show', $event ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show( $slug ) {
        $event = Event::findBySlugOrFail( $slug );
        return view( 'program::admin.events.show', compact( 'event' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit( $slug ) {
        $event = Event::findBySlugOrFail( $slug );
        $rooms = Room::orderBy( 'name' )->get();
        return view( 'program::admin.events.edit', compact( 'event', 'rooms' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $slug ) {

        $rooms = Room::select('id')->get()->pluck('id')->all();
        $rooms[] = "null";

        $this->validate( $request, [
            'name' => 'required',
            'start_time' => 'required|date',
            'end_time' => 'required|date',
            'room_id' => 'in:'.implode(",",$rooms),
            'data.description' => 'required_without:secondary',
        ] );

        $event = Event::findBySlugOrFail( $slug );

        $input = $request->all();
        $input['start_time'] = new Carbon($input['start_time']);
        $input['end_time'] = new Carbon($input['end_time']);
        $input['secondary'] = array_key_exists('secondary', $input);
        if($input['room_id'] == 'null'){
            $input['room_id'] = null;
        }

        $event->update( $input );
        $event->save();

        Flash::success( trans( 'program::program.events.edit_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@show', $event ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy( $slug ) {
        $event = Event::findBySlugOrFail( $slug );
        $event->delete();

        Flash::success( trans( 'program::program.events.remove_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@index' ) );
    }
}
