<?php

namespace EConf\ProgramManagement\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SessionType;
use App\Topic;
use Carbon\Carbon;
use EConf\ProgramManagement\Event;
use EConf\ProgramManagement\ProgramHelpers;
use EConf\ProgramManagement\Room;
use EConf\ProgramManagement\Session;
use Flash;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

class SessionsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $sessions = Session::select( DB::raw( 'sessions.*' ) )->join( 'events', 'sessions.id', '=', 'events.session_id' )->orderBy( 'events.start_time' )->get();
        return view( 'program::admin.sessions.index', compact( 'sessions' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $rooms = Room::orderBy( 'name' )->get();
        $session_types = SessionType::orderBy( 'name' )->get();
        $topics = Topic::orderBy( 'name' )->get();
        return view( 'program::admin.sessions.create', compact( 'rooms', 'session_types', 'topics' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        $this->validate( $request, [
            'start_time' => 'required|date',
            'end_time' => 'required|date',
            'room_id' => 'exists:rooms,id',
            'topic_id' => 'exists:topics,id',
            'session_type_id' => 'required|exists:session_types,id',
            'data.per_session' => 'integer|min:0',
        ] );

        $session = Session::create( $request->all() );

        $input = $request->only( [ 'start_time', 'end_time', 'room_id' ] );
        $input['start_time'] = new Carbon( $input['start_time'] );
        $input['end_time'] = new Carbon( $input['end_time'] );

        $event = new Event( $input );
        $event->session_id = $session->id;
        $event->save();

        Flash::success( trans( 'program::program.sessions.add_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@show', $session ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        $session = Session::findOrFail( $id );

        if ( ProgramHelpers::hasSubmissions() ) {
            $available_submissions = \EConf\Submissions\Submission::whereAccepted( true )->whereNotIn( 'id', function ( $query ) {
                $query->select( 'submission_id' )
                      ->from( 'session_submission' );
            } )->orderBy( 'title' )->get();
        } else {
            $available_submissions = null;
        }

        return view( 'program::admin.sessions.show', compact( 'session', 'available_submissions' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        $session = Session::findOrFail( $id );
        $rooms = Room::orderBy( 'name' )->get();
        $session_types = SessionType::orderBy( 'name' )->get();
        $topics = Topic::orderBy( 'name' )->get();
        return view( 'program::admin.sessions.edit', compact( 'session', 'rooms', 'session_types', 'topics' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {

        $this->validate( $request, [
            'start_time' => 'required|date',
            'end_time' => 'required|date',
            'room_id' => 'exists:rooms,id',
            'topic_id' => 'exists:topics,id',
            'session_type_id' => 'required|exists:session_types,id',
            'data.per_session' => 'integer|min:0',
        ] );

        $session = Session::findOrFail( $id );
        $session->update( $request->all() );
        $session->save();

        $input = $request->only( [ 'start_time', 'end_time', 'room_id' ] );
        $input['start_time'] = new Carbon( $input['start_time'] );
        $input['end_time'] = new Carbon( $input['end_time'] );

        $event = $session->event;
        $event->update( $input );
        $event->save();

        Flash::success( trans( 'program::program.sessions.edit_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@show', $session ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        $session = Session::findOrFail( $id );
        $session->delete();

        Flash::success( trans( 'program::program.sessions.remove_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@index' ) );
    }

    public function addSubmission( Request $request, $id ) {

        $session = Session::findOrFail( $id );

        $available_submissions = \EConf\Submissions\Submission::whereAccepted( true )->whereNotIn( 'id', function ( $query ) {
            $query->select( 'submission_id' )
                  ->from( 'session_submission' );
        } )->get()->pluck( 'id' );

        $this->validate( $request, [
            'submission_id' => 'in:' . ( $available_submissions->implode( ',' ) )
        ] );

        $session->submissions()->attach($request->submission_id);

        Flash::success( trans('program::program.sessions.submission_add_successful'));

        return redirect(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@show', $session));

    }

    public function removeSubmission( Request $request, $id, $subm_id ) {

        $session = Session::findOrFail( $id );

        $submission = $session->submissions()->findOrFail( $subm_id);

        $session->submissions()->detach($subm_id);

        Flash::success( trans('program::program.sessions.submission_remove_successful'));

        return redirect(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@show', $session));

    }

    public function moveSubmission( Request $request, $id, $subm_id ) {

        $session = Session::findOrFail( $id );

        $submission = $session->submissions()->findOrFail( $subm_id);

        $this->validate($request, [
            'dir' => 'required|in:up,down'
        ]);

        $currentPos = $submission->pivot->position;

        $query = $session->submissions();

        if($request->dir == "up"){
            $query->where('position', '<', $currentPos)->orderBy('pivot_position', 'desc');
            $moveDir = 'moveBefore';
        } else {
            $query->where('position', '>', $currentPos)->orderBy('pivot_position', 'asc');
            $moveDir = 'moveAfter';
        }

        $otherSubmission = $query->firstOrFail();

        $session->submissions()->move( $moveDir, $submission, $otherSubmission);

        return redirect(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@show', $session));

    }
}
