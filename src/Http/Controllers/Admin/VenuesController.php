<?php

namespace EConf\ProgramManagement\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use EConf\ProgramManagement\Venue;
use Eventy;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;

class VenuesController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $venues = Venue::orderBy( 'name' )->get();
        return view( 'program::admin.venues.index', compact( 'venues' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view( 'program::admin.venues.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        $this->validate( $request, [
            'name' => 'required',
            'address' => 'required'
        ] );

        $venue = Venue::create( $request->all() );
        $venue->save();

       Flash::success( trans( 'program::program.venues.add_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@show', $venue ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show( $slug ) {
        $venue = Venue::findBySlugOrFail( $slug );

        // Add assets
        Eventy::addFilter( 'admin.assets', function ( $val ) {
            $val[] = 'bower_components/leaflet/dist/leaflet.css';
            $val[] = 'bower_components/leaflet/dist/leaflet.js';
            return $val;
        }, 40, 1 );

        return view( 'program::admin.venues.show', compact( 'venue' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit( $slug ) {
        $venue = Venue::findBySlugOrFail( $slug );
        return view( 'program::admin.venues.edit', compact( 'venue' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $slug ) {

        $this->validate( $request, [
            'name' => 'required',
            'address' => 'required'
        ] );

        $venue = Venue::findBySlugOrFail( $slug );
        $venue->update( $request->all() );
        $venue->save();

        Flash::success( trans( 'program::program.venues.edit_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@show', $venue ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy( $slug ) {
        $venue = Venue::findBySlugOrFail( $slug );
        $venue->delete();

        Flash::success( trans( 'program::program.venues.remove_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@index' ) );
    }
}
