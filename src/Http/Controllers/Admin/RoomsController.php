<?php

namespace EConf\ProgramManagement\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use EConf\ProgramManagement\Room;
use EConf\ProgramManagement\Venue;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;

class RoomsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rooms = Room::orderBy( 'name' )->get();
        return view( 'program::admin.rooms.index', compact( 'rooms' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $venues = Venue::orderBy('name')->get();
        return view( 'program::admin.rooms.create', compact('venues') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        $this->validate( $request, [
            'name' => 'required',
            'capacity' => 'required|integer'
        ] );

        $room = Room::create( $request->all() );
        $room->save();

        Flash::success( trans( 'program::program.rooms.add_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@show', $room ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show( $slug ) {
        $room = Room::findBySlugOrFail( $slug );
        return view( 'program::admin.rooms.show', compact( 'room' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit( $slug ) {
        $room = Room::findBySlugOrFail( $slug );
        $venues = Venue::orderBy('name')->get();
        return view( 'program::admin.rooms.edit', compact( 'room', 'venues' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $slug ) {

        $this->validate( $request, [
            'name' => 'required',
            'capacity' => 'required|integer'
        ] );

        $room = Room::findBySlugOrFail( $slug );
        $room->update( $request->all() );
        $room->save();

        Flash::success( trans( 'program::program.rooms.edit_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@show', $room ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy( $slug ) {
        $room = Room::findBySlugOrFail( $slug );
        $room->delete();

        Flash::success( trans( 'program::program.rooms.remove_successful' ) );

        return redirect( m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@index' ) );
    }
}
