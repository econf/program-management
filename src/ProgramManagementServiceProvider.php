<?php

namespace EConf\ProgramManagement;

use App\Committee;
use EConf\ProgramManagement\Assignment\Chronos;
use EConf\ProgramManagement\Assignment\Dummy;
use Eventy;
use Illuminate\Support\ServiceProvider;

class ProgramManagementServiceProvider extends ServiceProvider {
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() {

        // Load routes
        if ( !$this->app->routesAreCached() ) {
            require __DIR__ . '/Http/routes.php';
        }

        // Load breadcrumbs
        require __DIR__ . '/Http/breadcrumbs.php';

        // Load views
        $this->loadViewsFrom( __DIR__ . '/resources/views', 'program' );

        // Load translations
        $this->loadTranslationsFrom( __DIR__ . '/resources/lang', 'program' );

        // Publish migrations
        $this->publishes( [
            __DIR__ . '/database/migrations/' => database_path( 'migrations' )
        ], 'migrations' );

        $this->addAdminMenu();

        $this->addPublicMenu();

        $this->registerAssignmentAlgorithms();

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register() {
        //
        $this->app->register( 'Toin0u\Geocoder\GeocoderServiceProvider' );

        $providers = [
            '\Geocoder\Provider\OpenStreetMapProvider' => [ \App::getLocale() ]
        ];

        config( ['geocoder.providers' => $providers] );
    }

    private function addAdminMenu() {
        Eventy::addAction( 'admin.menu', function ( $menu ) {
            if ( Committee::in( 'organizing' ) ) {
                $menu->raw( trans( 'program::program.label' ), [ 'class' => 'header' ] );
                $menu->add( trans( 'program::program.venues.label' ), m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@index' ) )
                     ->prepend( '<span class="fa fa-building"></span>' );
                $menu->add( trans( 'program::program.rooms.label' ), m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@index' ) )
                     ->prepend( '<span class="fa fa-cube"></span>' );
                $menu->add( trans( 'program::program.sessions.label' ), m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@index' ) )
                     ->prepend( '<span class="fa fa-list"></span>' );
                $menu->add( trans( 'program::program.events.label' ), m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@index' ) )
                     ->prepend( '<span class="fa fa-calendar-o"></span>' );
                if(ProgramHelpers::hasSubmissions()){
                    $menu->add( trans( 'program::program.assignment.label' ), m_action( '\EConf\ProgramManagement\Http\Controllers\Admin\AssignmentController@auto_show' ) )
                         ->prepend( '<span class="fa fa-magic"></span>' );
                }
            }
        }, 400, 1 );
    }

    private function addPublicMenu() {
        Eventy::addAction( 'public.menu', function ( $menu ) {
            $menu->add( trans( 'program::program.program' ), m_action( '\EConf\ProgramManagement\Http\Controllers\PublicController@program' ) );
        }, 40, 1 );
    }

    private function registerAssignmentAlgorithms() {
        Eventy::addFilter( 'program.assignment.algorithms', function ( $algos ) {
            if ( config('app.debug') ) {
                $dummy = new Dummy();
                $algos[$dummy->getSlug()] = get_class( $dummy );
            }

            $chronos = new Chronos();
            $algos[$chronos->getSlug()] = get_class( $chronos );
            return $algos;
        }, 40, 1 );
    }
}
