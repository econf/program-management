<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 02/07/16
 * Time: 17:24
 */

namespace EConf\ProgramManagement;


class ProgramHelpers {

    private static $social = [
        'website' => [
            'name' => 'Website',
            'icon' => 'globe'
        ],
        'facebook' => [
            'name' => 'Facebook',
            'icon' => 'facebook'
        ],
        'twitter' => [
            'name' => 'Twitter',
            'icon' => 'twitter'
        ],
        'google-plus' => [
            'name' => 'Google+',
            'icon' => 'google-plus'
        ],
        'instagram' => [
            'name' => 'Instagram',
            'icon' => 'instagram',
        ],
        'linkedin' => [
            'name' => 'LinkedIn',
            'icon' => 'linkedin'
        ],
        'github' => [
            'name' => 'GitHub',
            'icon' => 'github'
        ],
        'gitlab' => [
            'name' => 'GitLab',
            'icon' => 'gitlab'
        ]

    ];

    public static function hasSubmissions(){
        return class_exists("\\EConf\\Submissions\\Submission");
    }

    /**
     * @return array
     */
    public static function getSocial() {
        return self::$social;
    }



}
