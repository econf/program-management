<?php

namespace EConf\ProgramManagement;

use App\Traits\HasDataFieldTrait;
use App\Traits\TenantableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Event extends Model{

    use TenantableTrait;
    use HasDataFieldTrait;
    use Sluggable;
    use SluggableScopeHelpers;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'start_time',
        'end_time',
        'room_id',
        'secondary',
        'data'
    ];

    protected $casts = [
        'data' => 'array',
        'secondary' => 'boolean'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at', 'start_time', 'end_time' ];

    public function getNameAttribute($value){
        if(is_null($this->session_id)){
            return $value;
        }
        return trans('program::program.sessions.session_about', ['topic' => $this->session->topic->name]);
    }

    public function session(){
        return $this->belongsTo(Session::class);
    }

    public function room(){
        return $this->belongsTo(Room::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName() {
        return 'slug';
    }

    public function overlaps(Event $event){
        if($this->start_time->lte($event->start_time)){
            $e1 = $this;
            $e2 = $event;
        } else {
            $e1 = $event;
            $e2 = $this;
        }

        return $e2->start_time->lt($e1->end_time);


    }
}
