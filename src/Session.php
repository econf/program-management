<?php

namespace EConf\ProgramManagement;

use App\SessionType;
use App\Topic;
use App\Traits\HasDataFieldTrait;
use App\Traits\TenantableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\BelongsToSortedManyTrait;

class Session extends Model {

    use TenantableTrait;
    use HasDataFieldTrait;
    use BelongsToSortedManyTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'topic_id',
        'session_type_id',
        'data',
    ];

    protected $casts = [
        'data' => 'array',
    ];

    public function topic() {
        return $this->belongsTo( Topic::class );
    }

    public function session_type() {
        return $this->belongsTo( SessionType::class );
    }

    public function event(){
        return $this->hasOne(Event::class);
    }

    public function submissions(){
        if(ProgramHelpers::hasSubmissions()){
            return $this->belongsToSortedMany('EConf\Submissions\Submission');
        }
        return null;
    }

    public function nSubmissionsInSession(){
        if($this->session_type->parallel){
            return $this->data('per_session', 10);
        }

        $each = $this->session_type->duration;
        $duration = $this->event->start_time->diffInMinutes($this->event->end_time);

        return floor($duration / $each);
    }

}
