<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('program::program.venues.venue_data') }}</h3>
    </div>
    <div class="box-body">
        {!! BootForm::text(trans('program::program.venues.name'), 'name') !!}
        {!! BootForm::textarea(trans('program::program.venues.address'), 'address')->rows(3) !!}
    </div>
</div>
