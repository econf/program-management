@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.venues.add_venue') }}
        </h1>
        {!! Breadcrumbs::render('program.venues.create') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@store')) !!}

        @include('program::admin.venues.form')

        {!! BootForm::submit(trans('econf.actions.add'), 'btn-primary') !!}
        {!! BootForm::close() !!}

    </section>

@endsection
