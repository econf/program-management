@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ $venue->name }}
            <small>{{ trans('program::program.venues.singular_label') }}</small>
        </h1>
        {!! Breadcrumbs::render('program.venues.show', $venue) !!}
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-7 col-lg-8">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('program::program.venues.venue_data') }}</h3>
                    </div>
                    <div class="box-body">

                        <p>
                            <strong>{{ trans('program::program.venues.name') }}</strong><br>
                            {{ $venue->name }}
                        </p>

                        <p>
                            <strong>{{ trans('program::program.venues.address') }}</strong><br>
                            <span style="white-space: pre-line">{{ $venue->address }}</span>
                        </p>

                    </div>
                    <div class="box-body no-padding" id="venueMap" style="height: 18rem;"></div>
                </div>
            </div>
            <div class="col-md-5 col-lg-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('program::program.rooms.label') }}</h3>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ trans('program::program.rooms.name') }}</th>
                                <th class="text-right">{{ trans('program::program.rooms.capacity') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($venue->rooms as $room)
                                <tr>
                                    <td>{{ $room->name }}</td>
                                    <td class="text-right">{{ $room->capacity }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@edit', $venue) }}"
               class="btn btn-default">
                <span class="fa fa-pencil"></span>
                {{ trans('program::program.venues.edit_venue') }}
            </a>
        </p>

    </section>

@endsection

@section('scripts')
    <script>
        var coords = [{{ $venue->latitude }},{{ $venue->longitude }}];
        var venueMap = L.map('venueMap').setView(coords, 15);

        @if(is_null(env('MAPBOX_API_KEY', null)))
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(venueMap);
        @else
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: '&copy; <a href="http://mapbox.com/about/maps">Mapbox</a> &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>',
            maxZoom: 18,
            id: '{{ env('MAPBOX_ID', 'mapbox/streets-v9') }}',
            accessToken: '{{ env('MAPBOX_API_KEY') }}'
        }).addTo(venueMap);
        @endif

        var marker = L.marker(coords).addTo(venueMap);
    </script>
@endsection
