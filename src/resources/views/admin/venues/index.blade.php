@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.venues.label') }}
        </h1>
        {!! Breadcrumbs::render('program.venues') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@create') }}" class="btn btn-default">
                <span class="fa fa-plus"></span>
                {{ trans('program::program.venues.add_venue') }}
            </a>
        </p>

        <div class="box box-default">
            <div class="box-body no-padding">

                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ trans('program::program.venues.name') }}</th>
                        <th>{{ trans('program::program.rooms.label') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($venues as $venue)
                        <tr>
                            <td>
                                <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@show', $venue) }}">
                                    {{ $venue->name }}
                                </a>
                            </td>
                            <td class="text-muted">
                                {{ $venue->rooms()->count() }}
                            </td>
                            <td class="text-right">
                                <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\VenuesController@edit', $venue) }}" class="btn btn-xs btn-default" title="{{ trans('econf.actions.edit') }}">
                                    <span class="fa fa-pencil"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </section>

@endsection
