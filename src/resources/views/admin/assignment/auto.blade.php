@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.assignment.label') }}
        </h1>
        {!! Breadcrumbs::render('program.assignment') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <div class="callout callout-info">
            <h4>{{ trans('program::program.assignment.about_title') }}</h4>
            <p>{{ trans('program::program.assignment.about_message_algos') }}</p>
            <p>{{ trans('program::program.assignment.about_message_method') }}</p>
        </div>

        @foreach($algorithms as $algorithm)
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">
                        {{ $algorithm->getName() }}
                    </h3>
                </div>
                <div class="box-body">
                    {{ $algorithm->getDescription() }}
                </div>
                <div class="box-footer">
                    <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\AssignmentController@auto_algo', $algorithm->getSlug()) }}"
                       class="btn btn-default">
                        {{ trans('program::program.assignment.generate') }}
                    </a>
                </div>
            </div>
        @endforeach

    </section>
@endsection
