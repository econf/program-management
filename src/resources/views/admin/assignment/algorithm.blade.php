@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ $algorithm->getName() }}
        </h1>
        {!! Breadcrumbs::render('program.assignment') !!}
    </section>

    <!-- Main content -->
    <section class="content assignment-auto">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\AssignmentController@auto_store')) !!}

        <div class="row">
            @foreach($assigns as $session_id => $subs)
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                {{ Date::instance($sessions->get($session_id)->event->start_time)->format(trans('econf.date.short')) }}
                            </h3>
                        </div>
                        <div class="box-body">
                            <ul>
                                @foreach($subs as $id)
                                    <?php $submission = $submissions->get( $id ); ?>
                                    <li>
                                        {{ $submission->title }}
                                        <small class="text-muted">
                                            {{ $submission->data('author')->implode('name',', ') }}
                                        </small>
                                        <input type="checkbox" name="asgn[{{$session_id}}][{{$id}}]" value="yes" checked style="display: none;">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>

        @if($existing_assignments)

            <div class="callout callout-warning">
                <h4>{{ trans('reviews::reviews.assignment.auto_warning_title') }}</h4>
                <p>{{ trans('reviews::reviews.assignment.auto_warning_message') }}</p>
            </div>

        @endif

        <div class="callout callout-info">
            <p>{{ trans('program::program.assignment.tune_message') }}</p>
        </div>

        {!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>
@endsection
