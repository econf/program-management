@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.events.label') }}
        </h1>
        {!! Breadcrumbs::render('program.events') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@create') }}"
               class="btn btn-default">
                <span class="fa fa-plus"></span>
                {{ trans('program::program.events.add_event') }}
            </a>
        </p>

        <div class="box box-default">
            <div class="box-body no-padding">

                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ trans('program::program.events.name') }}</th>
                        <th>{{ trans('program::program.events.start_time') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $event)
                        <tr>
                            <td>
                                @if(is_null($event->session_id))
                                    <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@show', $event) }}">
                                        {{ $event->name }}
                                    </a>
                                @else
                                    {{ $event->name }}
                                @endif
                            </td>
                            <td>
                                {{ Date::instance($event->start_time)->format(trans('econf.date.medium')) }}
                            </td>
                            <td class="text-right">
                                @if(is_null($event->session_id))
                                    <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@edit', $event) }}"
                                       class="btn btn-xs btn-default" title="{{ trans('econf.actions.edit') }}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                @else
                                    <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@show', $event->session) }}"
                                       class="btn btn-xs btn-default"
                                       title="{{ trans('program::program.sessions.singular_label') }}">
                                        <span class="fa fa-list"></span>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </section>

@endsection
