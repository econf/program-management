@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.events.edit_event') }}
        </h1>
        {!! Breadcrumbs::render('program.events.edit') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@update', $event))->put() !!}
        {!! BootForm::bind($event) !!}

        @include('program::admin.events.form')

        {!! BootForm::submit(trans('econf.actions.edit'), 'btn-primary') !!}
        <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@show', $event) }}"
           class="btn btn-default">
            {{ trans('econf.actions.cancel') }}
        </a>

        <div class="pull-right">
            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#removeModal">
                {{ trans('econf.actions.remove') }}
            </a>
        </div>


        {!! BootForm::close() !!}

    </section>


    <div class="modal modal-danger" id="removeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">{{ trans('program::program.events.remove_event') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{!! trans('program::program.events.remove_confirmation') !!}</p>
                    <p>{!! trans('econf.layout.delete_warning') !!}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                        {{ trans('econf.actions.cancel') }}
                    </button>
                    {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@destroy', $event))->delete() !!}
                    {!! BootForm::submit(trans('econf.actions.remove'), 'btn-outline') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
