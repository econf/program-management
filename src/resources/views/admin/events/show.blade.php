@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ $event->name }}
            <small>{{ trans('program::program.events.singular_label') }}</small>
        </h1>
        {!! Breadcrumbs::render('program.events.show', $event) !!}
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('program::program.events.event_data') }}</h3>
            </div>
            <div class="box-body">

                <p>
                    <strong>{{ trans('program::program.events.name') }}</strong><br/>
                    {{ $event->name }}
                </p>

                <p>
                    <strong>{{ trans('program::program.events.start_time') }}</strong><br/>
                    {{ Date::instance($event->start_time)->format(trans('econf.date.fullDate') . ' - ' . trans('econf.date.time')) }}
                </p>

                <p>
                    <strong>{{ trans('program::program.events.end_time') }}</strong><br/>
                    {{ Date::instance($event->end_time)->format(trans('econf.date.fullDate') . ' - ' . trans('econf.date.time')) }}
                </p>

                <p>
                    <strong>{{ trans('program::program.events.duration') }}</strong><br/>
                    {{ Date::instance($event->start_time)->timespan($event->end_time) }}
                </p>

                @if(!is_null($event->room))
                    <p>
                        <strong>{{ trans('program::program.rooms.singular_label') }}</strong><br/>
                        {{ $event->room->name }}<br/>
                        <span class="text-muted">{{ $event->room->venue->name }}</span>
                    </p>
                @endif

                <p>
                    <strong>{{ trans('program::program.events.secondary') }}</strong>
                    @if($event->secondary)
                        <span class="fa fa-check-circle text-success"></span>
                    @else
                        <span class="fa fa-times-circle text-danger"></span>
                    @endif
                </p>

            </div>
        </div>

        @unless($event->secondary)
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('program::program.events.description') }}</h3>
                </div>
                <div class="box-body">
                    {!! $event->data('description') !!}
                </div>
            </div>
        @endunless

        @unless(empty($event->data('speaker.name')))
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        {{ trans('program::program.events.speaker.info') }}
                    </h3>
                </div>
                <div class="box-body">
                    <p>
                        <strong>{{ $event->data('speaker.name') }}</strong>
                        @if($event->data('speaker.organization'))
                            <br/>
                            @if($event->data('speaker.role'))
                                {{ $event->data('speaker.role') }},
                            @endif
                            {{ $event->data('speaker.organization') }}
                        @endif
                    </p>

                    @if($event->data('speaker.bio'))
                        <p>
                            {{ $event->data('speaker.bio') }}
                        </p>
                    @endif

                    @if($event->data('speaker.social'))
                        <div class="btn-group">
                            @foreach($event->data('speaker.social') as $slug => $url)
                                @unless(empty($url))
                                    <?php $social = \EConf\ProgramManagement\ProgramHelpers::getSocial()[$slug]; ?>
                                    <a href="{{ $url }}" class="btn btn-default" title="{{ $social['name'] }}">
                                        <span class="fa fa-fw fa-{{ $social['icon'] }}"></span>
                                    </a>
                                @endunless
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        @endunless

        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@edit', $event) }}"
               class="btn btn-default">
                <span class="fa fa-pencil"></span>
                {{ trans('program::program.events.edit_event') }}
            </a>
        </p>

    </section>

@endsection
