<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('program::program.events.event_data') }}</h3>
    </div>
    <div class="box-body">

        {!! BootForm::text(trans('program::program.events.name'), 'name') !!}
        @if(isset($event))
            {!! BootForm::text(trans('program::program.events.start_time'), 'start_time')->type('datetime-local')->value($event->start_time->format('Y-m-d\\TH:i:s')) !!}
            {!! BootForm::text(trans('program::program.events.end_time'), 'end_time')->type('datetime-local')->value($event->end_time->format('Y-m-d\\TH:i:s')) !!}
        @else
            {!! BootForm::text(trans('program::program.events.start_time'), 'start_time')->type('datetime-local') !!}
            {!! BootForm::text(trans('program::program.events.end_time'), 'end_time')->type('datetime-local') !!}
        @endif
        {!! BootForm::select(trans('program::program.rooms.singular_label'), 'room_id', $rooms->pluck('name', 'id'))->addOption('null', trans('program::program.rooms.no_room'))->defaultValue('null') !!}

        {!! BootForm::checkbox(trans('program::program.events.secondary'), 'secondary') !!}

        {!! BootForm::textarea(trans('program::program.events.description'), 'data[description]')->addClass('koala-simple')->helpBlock(trans('program::program.events.required_if_primary')) !!}

    </div>
</div>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ trans('program::program.events.speaker.info') }}
        </h3>
    </div>
    <div class="box-body">
        {!! BootForm::text(trans('program::program.events.speaker.name'), 'data[speaker][name]') !!}
        {!! BootForm::text(trans('program::program.events.speaker.organization'), 'data[speaker][organization]') !!}
        {!! BootForm::text(trans('program::program.events.speaker.role'), 'data[speaker][role]') !!}

        {!! BootForm::textarea(trans('program::program.events.speaker.short_bio'), 'data[speaker][bio]')->rows(2) !!}

        @foreach(\EConf\ProgramManagement\ProgramHelpers::getSocial() as $slug => $data)
            {!! BootForm::inputGroup($data['name'], "data[speaker][social][{$slug}]")->type('url')->beforeAddon("<span class=\"fa fa-fw fa-{$data['icon']}\"></span>") !!}
        @endforeach
    </div>
</div>
