@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.events.add_event') }}
        </h1>
        {!! Breadcrumbs::render('program.events.create') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\EventsController@store')) !!}

        @include('program::admin.events.form')

        {!! BootForm::submit(trans('econf.actions.add'), 'btn-primary') !!}
        {!! BootForm::close() !!}

    </section>

@endsection
