@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.sessions.label') }}
        </h1>
        {!! Breadcrumbs::render('program.sessions') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@create') }}" class="btn btn-default">
                <span class="fa fa-plus"></span>
                {{ trans('program::program.sessions.add_session') }}
            </a>
        </p>

        <div class="box box-default">
            <div class="box-body no-padding">

                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ trans('program::program.events.start_time') }}</th>
                        <th>{{ trans('program::program.sessions.session_type') }}</th>
                        <th class="hidden-xs">{{ trans('program::program.sessions.topic') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sessions as $session)
                        <tr>
                            <td>
                                <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@show', $session) }}">
                                    {{ Date::instance($session->event->start_time)->format(trans('econf.date.medium')) }}
                                </a>
                            </td>
                            <td class="text-muted">
                                {{ $session->session_type->name }}
                            </td>
                            <td class="hidden-xs">
                                {{ $session->topic->name }}
                            </td>
                            <td class="text-right">
                                <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@edit', $session) }}" class="btn btn-xs btn-default" title="{{ trans('econf.actions.edit') }}">
                                    <span class="fa fa-pencil"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </section>

@endsection
