@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.sessions.add_session') }}
        </h1>
        {!! Breadcrumbs::render('program.sessions.create') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@store')) !!}

        @include('program::admin.sessions.form')

        {!! BootForm::submit(trans('econf.actions.add'), 'btn-primary') !!}
        {!! BootForm::close() !!}

    </section>

@endsection
