<div class="row">

    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('program::program.events.event_data') }}</h3>
            </div>
            <div class="box-body">
                @if(isset($session))
                    {!! BootForm::text(trans('program::program.events.start_time'), 'start_time')->type('datetime-local')->value($session->event->start_time->format('Y-m-d\\TH:i:s')) !!}
                    {!! BootForm::text(trans('program::program.events.end_time'), 'end_time')->type('datetime-local')->value($session->event->end_time->format('Y-m-d\\TH:i:s')) !!}
                    {!! BootForm::select(trans('program::program.rooms.singular_label'), 'room_id', $rooms->pluck('name', 'id'))->select($session->event->room_id) !!}
                @else
                    {!! BootForm::text(trans('program::program.events.start_time'), 'start_time')->type('datetime-local') !!}
                    {!! BootForm::text(trans('program::program.events.end_time'), 'end_time')->type('datetime-local') !!}
                    {!! BootForm::select(trans('program::program.rooms.singular_label'), 'room_id', $rooms->pluck('name', 'id')) !!}
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('program::program.sessions.session_data') }}</h3>
            </div>
            <div class="box-body">
                {!! BootForm::select(trans('program::program.sessions.session_type'), 'session_type_id', $session_types->pluck('name', 'id')) !!}
                {!! BootForm::select(trans('program::program.sessions.topic'), 'topic_id', $topics->pluck('name', 'id')) !!}
                {!! BootForm::text(trans('program::program.sessions.moderator'), 'data[moderator]') !!}
                {!! BootForm::text(trans('program::program.sessions.per_session'), 'data[per_session]')->type('number')->min(0)->step(1)->helpBlock(trans('program::program.sessions.per_session_help')) !!}
            </div>
        </div>
    </div>

</div>
