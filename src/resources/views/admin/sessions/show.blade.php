@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.sessions.singular_label') }}
        </h1>
        {!! Breadcrumbs::render('program.sessions.show') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('program::program.events.event_data') }}</h3>
                    </div>
                    <div class="box-body">

                        <p>
                            <strong>{{ trans('program::program.events.start_time') }}</strong><br/>
                            {{ Date::instance($session->event->start_time)->format(trans('econf.date.fullDate') . ' - ' . trans('econf.date.time')) }}
                        </p>

                        <p>
                            <strong>{{ trans('program::program.events.end_time') }}</strong><br/>
                            {{ Date::instance($session->event->end_time)->format(trans('econf.date.fullDate') . ' - ' . trans('econf.date.time')) }}
                        </p>

                        <p>
                            <strong>{{ trans('program::program.events.duration') }}</strong><br/>
                            {{ Date::instance($session->event->start_time)->timespan($session->event->end_time) }}
                        </p>

                        <p>
                            <strong>{{ trans('program::program.rooms.singular_label') }}</strong><br/>
                            {{ $session->event->room->name }}<br/>
                            <span class="text-muted">{{ $session->event->room->venue->name }}</span>
                        </p>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('program::program.sessions.session_data') }}</h3>
                    </div>
                    <div class="box-body">

                        <p>
                            <strong>{{ trans('program::program.sessions.session_type') }}</strong><br/>
                            {{ $session->session_type->name }}<br/>
                            <span class="text-muted">
                                {{ $session->session_type->duration }}
                                @if($session->session_type->parallel)
                                    {{ trans('econf.session_types.minutes') }}
                                    ({{ trans('econf.session_types.parallel') }} -
                                    {{ trans('program::program.sessions.num_submissions', ['num' => $session->nSubmissionsInSession()]) }})
                                @else
                                    {{ trans('econf.session_types.minutes_each') }}
                                    ({{ trans('program::program.sessions.num_submissions', ['num' => $session->nSubmissionsInSession()]) }}
                                    )
                                @endif
                            </span>
                        </p>

                        <p>
                            <strong>{{ trans('program::program.sessions.topic') }}</strong><br/>
                            {{ $session->topic->name }}
                        </p>

                        @unless(empty($session->data('moderator')))
                            <p>
                                <strong>{{ trans('program::program.sessions.moderator') }}</strong><br/>
                                {{ $session->data('moderator') }}
                            </p>
                        @endunless

                    </div>
                </div>

                @if(\EConf\ProgramManagement\ProgramHelpers::hasSubmissions())
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ trans('program::program.sessions.submissions') }}</h3>
                        </div>
                        @if($session->submissions->isEmpty())
                            <div class="box-body text-muted text-center">
                                {{ trans('program::program.sessions.no_submissions') }}
                            </div>
                        @else
                            <div class="box-body no-padding">
                                <table class="table">
                                    <tbody>
                                    @foreach($session->submissions as $submission)
                                        <tr>
                                            <td>{{ $submission->title }}</td>
                                            <td class="text-right">
                                                <form
                                                    action="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@moveSubmission', ['id' => $session, 'subm_id' => $submission->id]) }}"
                                                    method="POST" class="move-form">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="btn-group btn-group-xs">
                                                        @unless($submission == $session->submissions->first())
                                                            <button class="btn btn-default" data-move name="dir"
                                                                    value="up"
                                                                    type="submit">
                                                                <span class="fa fa-arrow-up"></span>
                                                            </button>
                                                        @endunless
                                                        @unless($submission == $session->submissions->last())
                                                            <button class="btn btn-default" data-move name="dir"
                                                                    value="down"
                                                                    type="submit">
                                                                <span class="fa fa-arrow-down"></span>
                                                            </button>
                                                        @endunless
                                                        <button class="btn btn-danger" type="button" data-toggle="modal"
                                                                href="#removeSub{{ $submission->id }}Modal">
                                                            <span class="fa fa-minus"></span>
                                                        </button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                        @if($session->submissions->count() < $session->nSubmissionsInSession() and !$available_submissions->isEmpty())
                            <div class="box-footer">
                                <button type="button" class="btn btn-default btn-sm" data-toggle="modal"
                                        href="#addSubModal">
                                    <span class="fa fa-plus"></span>
                                    {{ trans('program::program.sessions.add_submission') }}
                                </button>
                            </div>
                        @endif
                    </div>
                @endif

            </div>

        </div>

        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@edit', $session) }}"
               class="btn btn-default">
                <span class="fa fa-pencil"></span>
                {{ trans('program::program.sessions.edit_session') }}
            </a>
        </p>

    </section>

    @if(\EConf\ProgramManagement\ProgramHelpers::hasSubmissions())
        @if($session->submissions->count() < $session->nSubmissionsInSession() and !$available_submissions->isEmpty())
            <div class="modal fade" id="addSubModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ trans('program::program.sessions.add_submission') }}</h4>
                        </div>
                        {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@addSubmission', $session))->put() !!}
                        <div class="modal-body">
                            {!! BootForm::select(trans('program::program.sessions.submission'), 'submission_id', $available_submissions->pluck('title', 'id'))->style('width:100%;')->class('select2') !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                                {{ trans('econf.actions.cancel') }}
                            </button>
                            {!! BootForm::submit(trans('econf.actions.add'), 'btn-primary') !!}
                        </div>
                        {!! BootForm::close() !!}
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        @endif

        @foreach($session->submissions as $submission)
            <div class="modal modal-danger" id="removeSub{{ $submission->id }}Modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">{{ trans('program::program.sessions.remove_submission') }}</h4>
                        </div>
                        <div class="modal-body">
                            <p>{!! trans('program::program.sessions.submission_remove_confirmation', ['name' => $submission->title]) !!}</p>
                            <p>{!! trans('econf.layout.delete_warning') !!}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                                {{ trans('econf.actions.cancel') }}
                            </button>
                            {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\SessionsController@removeSubmission', ['id' => $session, 'subm_id' => $submission->id]))->delete() !!}
                            {!! BootForm::submit(trans('econf.actions.remove'), 'btn-outline') !!}
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endforeach
    @endif

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.move-form button[data-move]', function (e) {
                var $button = $(this);
                var $form = $(this).parents('form');
                var $input = $('<input type="hidden" name="dir" />');
                $input.val($button.val());
                $form.append($input);
            });
        });
    </script>
@endsection
