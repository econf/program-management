@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.rooms.add_room') }}
        </h1>
        {!! Breadcrumbs::render('program.rooms.create') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        {!! BootForm::open()->action(m_action('\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@store')) !!}

        @include('program::admin.rooms.form')

        {!! BootForm::submit(trans('econf.actions.add'), 'btn-primary') !!}
        {!! BootForm::close() !!}

    </section>

@endsection
