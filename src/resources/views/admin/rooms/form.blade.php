<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('program::program.rooms.room_data') }}</h3>
    </div>
    <div class="box-body">
        {!! BootForm::text(trans('program::program.rooms.name'), 'name') !!}
        {!! BootForm::text(trans('program::program.rooms.location_info'), 'data[location_info]')->helpBlock(trans('program::program.rooms.location_info_hint')) !!}
        {!! BootForm::text(trans('program::program.rooms.capacity'), 'capacity')->type('number')->min(1)->step(1) !!}
        {!! BootForm::select(trans('program::program.venues.singular_label'), 'venue_id', $venues->pluck('name', 'id')) !!}
    </div>
</div>
