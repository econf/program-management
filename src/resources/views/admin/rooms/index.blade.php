@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('program::program.rooms.label') }}
        </h1>
        {!! Breadcrumbs::render('program.rooms') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@create') }}" class="btn btn-default">
                <span class="fa fa-plus"></span>
                {{ trans('program::program.rooms.add_room') }}
            </a>
        </p>

        <div class="box box-default">
            <div class="box-body no-padding">

                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ trans('program::program.rooms.name') }}</th>
                        <th>{{ trans('program::program.rooms.capacity') }}</th>
                        <th class="hidden-xs">{{ trans('program::program.venues.singular_label') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rooms as $room)
                        <tr>
                            <td>
                                <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@show', $room) }}">
                                    {{ $room->name }}
                                </a>
                            </td>
                            <td class="text-muted">
                                {{ $room->capacity }}
                            </td>
                            <td class="hidden-xs">{{ $room->venue->name }}</td>
                            <td class="text-right">
                                <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@edit', $room) }}" class="btn btn-xs btn-default" title="{{ trans('econf.actions.edit') }}">
                                    <span class="fa fa-pencil"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </section>

@endsection
