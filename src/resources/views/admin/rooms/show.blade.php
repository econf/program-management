@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ $room->name }}
            <small>{{ trans('program::program.rooms.singular_label') }}</small>
        </h1>
        {!! Breadcrumbs::render('program.rooms.show', $room) !!}
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('program::program.rooms.room_data') }}</h3>
            </div>
            <div class="box-body">

                <p>
                    <strong>{{ trans('program::program.rooms.name') }}</strong><br>
                    {{ $room->name }}
                </p>

                <p>
                    <strong>{{ trans('program::program.rooms.location_info') }}</strong><br>
                    {{ $room->data('location_info') }}
                </p>

                <p>
                    <strong>{{ trans('program::program.rooms.capacity') }}</strong><br>
                    {{ $room->capacity }}
                </p>

                <p>
                    <strong>{{ trans('program::program.venues.singular_label') }}</strong><br>
                    {{ $room->venue->name }}
                </p>

            </div>
        </div>

        <p>
            <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\Admin\RoomsController@edit', $room) }}" class="btn btn-default">
                <span class="fa fa-pencil"></span>
                {{ trans('program::program.rooms.edit_room') }}
            </a>
        </p>

    </section>

@endsection
