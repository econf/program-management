@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-clock-o"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ $event->name }}</h1>
                </div>
            </div>
        </div>

    </section>

    <div class="container">

        @include('flash::message')

        <div class="row">

            @if($event->session)
                <div class="col-md-8 col-lg-9">

                    @foreach($event->session->submissions as $submission)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ $submission->title }}</h3>
                            </div>
                            @if($submission->data('abstract'))
                                <div class="panel-body">
                                    {{ $submission->data('abstract') }}
                                </div>
                            @endif
                            <div class="panel-body">
                                <strong>{{ trans('submissions::submissions.form.authors') }}:</strong>
                                {{ $submission->data('author')->implode('name') }}
                            </div>
                            @if(!is_null($submission->data('presenter')))
                                <div class="panel-body">
                                    <strong>{{ trans('submissions::submissions.after_acceptance.presenter') }}:</strong>
                                    {{ $submission->data('author.'.$submission->data('presenter').'.name') }}
                                </div>
                            @endif
                        </div>
                    @endforeach

                </div>
            @elseif($event->data('speaker.name'))
                <div class="col-md-3">
                    <h3 class="page-header">
                        {{ trans('program::program.events.speaker.label') }}
                    </h3>

                    <div class="person-info">
                        <h2>{{ $event->data('speaker.name') }}</h2>
                        @if($event->data('speaker.organization'))
                            <p>
                                @if($event->data('speaker.role'))
                                    {{ $event->data('speaker.role') }},
                                @endif
                                {{ $event->data('speaker.organization') }}
                            </p>
                        @endif
                    </div>

                    @if($event->data('speaker.bio'))
                        <p class="text-muted">
                            <small>{{ $event->data('speaker.bio') }}</small>
                        </p>
                    @endif

                    <div class="social-icons social-icons-left">
                        <ul class="nav nav-pills">
                            @foreach($event->data('speaker.social') as $slug => $url)
                                @unless(empty($url))
                                    <?php $social = \EConf\ProgramManagement\ProgramHelpers::getSocial()[$slug]; ?>
                                    <li>
                                        <a href="{{ $url }}" class="{{ $slug }}" title="{{ $social['name'] }}">
                                            <span class="fa fa-fw fa-{{ $social['icon'] }}"></span>
                                        </a>
                                    </li>
                                @endunless
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-lg-6">
                    {!! $event->data('description') !!}
                </div>
            @else
                <div class="col-md-8 col-lg-9">
                    {!! $event->data('description') !!}
                </div>
            @endif


            <div class="col-md-4 col-lg-3 text-right">

                @if($event->session and !empty($event->session->data('moderator')))

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('program::program.sessions.moderator') }}</h3>
                        </div>
                        <div class="panel-body">
                            {{ $event->session->data('moderator') }}
                        </div>
                    </div>

                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-clock-o"></span></h3>
                    </div>
                    <div class="panel-body">
                        {{ Date::instance($event->start_time)->format(trans('econf.date.fullDate')) }}<br>
                        {{ $event->start_time->format(trans('econf.date.time')) }} -
                        {{ $event->end_time->format(trans('econf.date.time')) }}
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-map-marker"></span></h3>
                    </div>
                    <div class="panel-body">
                        {{ $event->room->name }}<br/>
                        <small class="text-muted">{{ $event->room->venue->name }}</small>
                    </div>
                    @unless(empty($event->room->data('location_info')))
                        <div class="panel-body text-muted">
                            {{ $event->room->data('location_info') }}
                        </div>
                    @endunless
                    <div id="venueMap" style="height: 14rem;"></div>
                </div>
            </div>

        </div>


    </div>

@endsection

@section('scripts')
    <script>
        var coords = [{{ $event->room->venue->latitude }},{{ $event->room->venue->longitude }}];
        var venueMap = L.map('venueMap').setView(coords, 14);

        @if(is_null(env('MAPBOX_API_KEY', null)))
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(venueMap);
        @else
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: '&copy; <a href="http://mapbox.com/about/maps">Mapbox</a> &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>',
            maxZoom: 18,
            id: '{{ env('MAPBOX_ID', 'mapbox/streets-v9') }}',
            accessToken: '{{ env('MAPBOX_API_KEY') }}'
        }).addTo(venueMap);
            @endif

        var marker = L.marker(coords).addTo(venueMap);
    </script>
@endsection
