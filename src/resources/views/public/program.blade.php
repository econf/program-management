@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-calendar"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ trans('program::program.program') }}</h1>
                </div>
            </div>
        </div>

    </section>

    <div class="container">

        @include('flash::message')

        @foreach($program as $day)
            <div class="program-day">
                <h2 class="page-header program-day__header">{{ Date::instance($day['date'])->format(trans('econf.date.fullDate')) }}</h2>

                <div class="program-day__events">
                    @foreach($day['events'] as $row)
                        <div class="program-day__row">
                            @foreach($row as $event)
                                <article class="program-day__event @if($event->secondary) secondary @endif">

                                    <h4 class="title event__name">{{ $event->name }}</h4>

                                    <p class="event__meta text-muted">
                                        <span class="event__time">
                                            <span class="fa fa-clock-o"></span>
                                            {{ $event->start_time->format(trans('econf.date.time')) }} -
                                            {{ $event->end_time->format(trans('econf.date.time')) }}
                                        </span>
                                        @if($event->room)
                                            <span class="event__room">
                                                <span class="fa fa-map-marker"></span>
                                                {{ $event->room->name }}
                                            </span>
                                        @endif
                                        @if($event->data('speaker.name'))
                                            <span class="event__speaker">
                                                <span class="fa fa-user"></span>
                                                {{ $event->data('speaker.name') }}
                                            </span>
                                        @endif
                                    </p>

                                    @unless($event->secondary)
                                        <a href="{{ m_action('\EConf\ProgramManagement\Http\Controllers\PublicController@event', $event->slug) }}"
                                           class="event__more">
                                            <span class="fa fa-plus-square-o"></span>
                                        </a>
                                    @endunless

                                </article>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach

    </div>

@endsection
