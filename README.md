# E-Conf Program Management

E-Conf is a Conference Management System designed to automate as much of the process as possible.

This package handles the program management phase of the conference organization. It is to be used with the [main E-Conf app][link-app-package].

![E-Conf Program Management](banner.png)

## Install

First, require this package via Composer:

``` bash
$ composer require econf/program-management
```

After updating Composer, add the Service Provider to the providers array in `config/app.php`.

``` php
EConf\ProgramManagement\ProgramManagementServiceProvider::class
```

Finally, you need to publish the package migrations and apply them to your database:

``` bash
$ php artisan vendor:publish --provider="EConf\ProgramManagement\ProgramManagementServiceProvider"
$ php artisan migrate
```

## Usage

You can prepare the program through the Program section on the administration panel of a conference.



## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[link-app-package]: https://gitlab.com/econf/econf
[link-author]: http://joaopluis.pt
